# Small Documentation

Although the scripts generates the documents itself. This document would generally describe the overall big picture.

Although I couldn't spent much time of the dataset, but a few observations could be stated:

__warmup.py__
* Overall data is more or less gender equal
* There are many countries with small number of users which are biased towards a particular gender in terms of total listening time
* Music tastes of Males and Females are different, there was just 17% overlap on the kind of tracks they listening

__stretching.py__

_Context_

I tried to get insights about the context

Males and Females in terms of the context they play from:

context| percentage_male |percentage_female      
---|---|---
album  |     13.849300      |   11.995323
app    |    2.387202        |  2.995789
artist |      17.433790      |   12.023821
collection |      12.261688   |      11.754095
me      |  1.427086        |  0.172372
playlist |      44.937446   |      55.632045
search    |    3.017011      |    1.746978
unknown    |    4.686476      |    3.679576


A few things we can say looking at this data is
1. Males are almost twice more likely to search than females
2. 'me' context is completely dominated by males
3. Females are more likely to play music via playlists



![](data/age_to_time.png)
Although this graph does not tell much but gives a small hint that new users tend to play more music (just a tiny bit more)

_Cluster_

I tried to cluster user behavior based on the count of context using k-means clustering.
A few interesting things that could be observed were:
* Age category 18-24 tend to listen more from collections than any other age category
* Females have almost twice more affinity towards playing a song from playlists
* Males have more affinity towards collections
* Gender has not much to do with `product` category
